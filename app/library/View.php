<?php

class View
{

    protected $val = false;

    function __construct()
    {
    }

    public static function getPage($val)
    {
        require '../app/views/' . $val . '.php';
    }

    public static function url($val)
    {
        echo '' . URL . '' . $val . '';
    }

    public static function segment($val) {

        if(isset($_GET['url'])){
            $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
            return $url[$val];

        }
    }

}

?>