<?php

class App {

    protected $url;
    protected $route_url;
    protected $controller_url;
    protected $controller_path;
    protected $controller;
    protected $method;
    protected $params = [];
    protected $short_url;
    protected $short_route_url;
    protected $route;


    public function __construct() {

        if(empty($_GET['url'])) {

            // Eğer URL yoksa homeController'daki index fonksiyonunu çalıştır.

            require '../app/controllers/user/homeController.php';
            $homeController = new homeController();
            $homeController->index();
        }

    }


    public function parseURL($val)
    {

        // Controller URL yolu
        $this->controller_url = '../app/controllers/' . $this->controller_path . $this->controller . '.php';

        // Eğer URL varsa parçala.
        if (isset($_GET['url'])) {
            $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }

        // Eğer $val değeri varsa.
        if (isset($val)) {
            $route_url = explode('/', filter_var(rtrim($val, '/'), FILTER_SANITIZE_URL));
        }

        @$this->url = $url;
        @$this->route_url = $route_url;
        @$this->short_url = array_slice($url, 0, 2);
        @$this->short_route_url = array_slice($route_url, 0, 2);
    }


    public function get($route, $controller_path = false, $controller, $method = false)

    {

        $this->method = $method;
        $this->controller_path = $controller_path;
        $this->controller = $controller;
        $this->route = $route;
        $this->parseURL($this->route);

        // Gelen URL ile rotalamadaki URL aynı ise işlem yap!
            if ($this->short_url == $this->short_route_url) {

                // Controller dosyası varsa
                if (file_exists($this->controller_url)) {
                    require $this->controller_url;
                    $this->controller = new $this->controller();
                    unset($this->url[0]);

                }

                if (isset($this->url[1])) {
                    if (method_exists($this->controller, $this->method)) {
                        unset($this->url[1]);
                    }
                }

                $this->params = $this->url ? array_values($this->url) : [];
                call_user_func_array([$this->controller, $this->method], $this->params);
            }
}

}

?>