<?php

class Controller {

    public function model($model, $path) {

     $model_url = '../app/models/'.$path.'/'.$model.'.php';

        if (file_exists($model_url)) {
            require $model_url;
            return new $model();
        }
    }

    public function view($view, $data = []) {

      $view_url = '../app/views/'.$view.'.php';

        if (file_exists($view_url)) {
            require $view_url;
        }
    }

}

?>