<?php


class homeController extends Controller {

    protected $index_model;

    public function __construct() {
        $this->index_model = $this->model('index_model', 'user');
    }

    public function index($param=false) {

        $index_model = $this->index_model;
        $index_model->index();
        $this->view('user/index', ['query' => $index_model->query]);
    }


    public function send($param=false) {
        $this->view('user/home/send', []);
    }

    public function welcome($param=false) {
        echo 'merhaba';
    }


}

?>