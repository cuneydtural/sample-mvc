<?php

class Test extends Controller {


    public function __construct() {
       $this->test_model = $this->model('test_model', 'admin');
    }

    public function index($val) {

        $test_model = $this->test_model;
        $test_model->index();
        $test_model->name = $val;

        $this->view('test/index', ['name' => $test_model->name]);

    }


}

?>