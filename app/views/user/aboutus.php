<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="<?php View::url('css/reset.css') ?>">
    <link rel="stylesheet" href="<?php View::url('css/style.css') ?>">
</head>
<body>



<section id="container">

    <?php View::getPage('user/header') ?>

    <div class="page-title">Hakkımızda</div>

    <div class="content">

    </div>


</section>

<script type="text/javascript" src="<?php View::url('js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript" src="<?php View::url('js/main.js') ?>"></script>

</body>
</html>
