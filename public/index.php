<?php

require_once '../app/library/App.php';
require_once '../app/library/Controller.php';
require_once '../app/library/Database.php';
require_once '../app/library/Model.php';
require_once '../app/library/View.php';
require_once '../app/config/database.php';
require_once '../app/config/paths.php';
require_once '../app/config/settings.php';
require_once '../vendor/autoload.php';
require_once '../app/routes.php';



?>